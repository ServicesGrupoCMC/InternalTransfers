
package com.bbva.ccol.transfers.business.dto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.GregorianCalendar;;



public class DTOIntInternalTransfer {

    public final static long serialVersionUID = 1L;
    
    private String internalTransferId;
    private String concept;
    private DTOIntSender sender;
    private DTOIntReceiver receiver;
    private List<DTOIntSentMoney> sentMoney;
    private DTOIntChargeAmount chargeAmount;
    private Date date;
    private Date modifiedDate;
    private Date accountingDate;
    private DTOIntInternalTransferFees fees;
    private DTOIntInternalTransferTaxes taxes;
    private DTOIntExchangeRate exchangeRate;
    private List<DTOIntReferences> references;
    private String customExternalReference;
    private String operationNumber;
    private List<DTOIntNotification> notification;
    private DTOIntStatus status;

    public DTOIntInternalTransfer() {
        //default constructor
    }

    public String getInternalTransferId() {
        return internalTransferId;
    }

    public void setInternalTransferId(String internalTransferId) {
        this.internalTransferId = internalTransferId;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public DTOIntSender getSender() {
        return sender;
    }

    public void setSender(DTOIntSender sender) {
        this.sender = sender;
    }

    public DTOIntReceiver getReceiver() {
        return receiver;
    }

    public void setReceiver(DTOIntReceiver receiver) {
        this.receiver = receiver;
    }

    public List<DTOIntSentMoney> getSentMoney() {
        return sentMoney;
    }

    public void setSentMoney(List<DTOIntSentMoney> sentMoney) {
        this.sentMoney = sentMoney;
    }

    public DTOIntChargeAmount getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(DTOIntChargeAmount chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Date getAccountingDate() {
        return accountingDate;
    }

    public void setAccountingDate(Date accountingDate) {
        this.accountingDate = accountingDate;
    }

    public DTOIntInternalTransferFees getFees() {
        return fees;
    }

    public void setFees(DTOIntInternalTransferFees fees) {
        this.fees = fees;
    }

    public DTOIntInternalTransferTaxes getTaxes() {
        return taxes;
    }

    public void setTaxes(DTOIntInternalTransferTaxes taxes) {
        this.taxes = taxes;
    }

    public DTOIntExchangeRate getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(DTOIntExchangeRate exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public List<DTOIntReferences> getReferences() {
        return references;
    }

    public void setReferences(List<DTOIntReferences> references) {
        this.references = references;
    }

    public String getCustomExternalReference() {
        return customExternalReference;
    }

    public void setCustomExternalReference(String customExternalReference) {
        this.customExternalReference = customExternalReference;
    }

    public String getOperationNumber() {
        return operationNumber;
    }

    public void setOperationNumber(String operationNumber) {
        this.operationNumber = operationNumber;
    }

    public List<DTOIntNotification> getNotification() {
        return notification;
    }

    public void setNotification(List<DTOIntNotification> notification) {
        this.notification = notification;
    }

    public DTOIntStatus getStatus() {
        return status;
    }

    public void setStatus(DTOIntStatus status) {
        this.status = status;
    }

}
