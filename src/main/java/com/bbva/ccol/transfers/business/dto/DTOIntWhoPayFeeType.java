
package com.bbva.ccol.transfers.business.dto;




public class DTOIntWhoPayFeeType {

    public final static long serialVersionUID = 1L;
    private String id;
    private String name;

    public DTOIntWhoPayFeeType() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
