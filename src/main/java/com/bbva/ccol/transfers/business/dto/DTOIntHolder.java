
package com.bbva.ccol.transfers.business.dto;

import java.util.List;



public class DTOIntHolder {

    public final static long serialVersionUID = 1L;
    private String holderName;
    private List<DTOIntIdentityDocument> identityDocuments;
    private String contactType;
    private String contactValue;

    public DTOIntHolder() {
        //default constructor
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public List<DTOIntIdentityDocument> getIdentityDocuments() {
        return identityDocuments;
    }

    public void setIdentityDocuments(List<DTOIntIdentityDocument> identityDocuments) {
        this.identityDocuments = identityDocuments;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getContactValue() {
        return contactValue;
    }

    public void setContactValue(String contactValue) {
        this.contactValue = contactValue;
    }

}
