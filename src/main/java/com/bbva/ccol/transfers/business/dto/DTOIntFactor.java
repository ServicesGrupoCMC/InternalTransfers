
package com.bbva.ccol.transfers.business.dto;




public class DTOIntFactor {

    public final static long serialVersionUID = 1L;
    private Double value;
    private Double ratio;

    public DTOIntFactor() {
        //default constructor
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Double getRatio() {
        return ratio;
    }

    public void setRatio(Double ratio) {
        this.ratio = ratio;
    }

}
