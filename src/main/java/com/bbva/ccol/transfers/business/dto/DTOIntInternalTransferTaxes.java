
package com.bbva.ccol.transfers.business.dto;

import java.util.List;



public class DTOIntInternalTransferTaxes {

    public final static long serialVersionUID = 1L;
    private DTOIntAmount totalTaxes;
    private List<DTOIntItemizeTaxes> itemizeTaxes;
    private Double percentage;

    public DTOIntInternalTransferTaxes() {
        //default constructor
    }

    public DTOIntAmount getTotalTaxes() {
        return totalTaxes;
    }

    public void setTotalTaxes(DTOIntAmount totalTaxes) {
        this.totalTaxes = totalTaxes;
    }

    public List<DTOIntItemizeTaxes> getItemizeTaxes() {
        return itemizeTaxes;
    }

    public void setItemizeTaxes(List<DTOIntItemizeTaxes> itemizeTaxes) {
        this.itemizeTaxes = itemizeTaxes;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }

}
