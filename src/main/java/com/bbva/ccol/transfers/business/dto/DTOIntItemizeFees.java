
package com.bbva.ccol.transfers.business.dto;




public class DTOIntItemizeFees {

    public final static long serialVersionUID = 1L;
    private String feeType;
    private String name;
    private DTOIntAmount amount;
    private Double percentage;
    private DTOIntWhoPayFeeType whoPayFee;

    public DTOIntItemizeFees() {
        //default constructor
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DTOIntAmount getAmount() {
        return amount;
    }

    public void setAmount(DTOIntAmount amount) {
        this.amount = amount;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }

    public DTOIntWhoPayFeeType getWhoPayFee() {
        return whoPayFee;
    }

    public void setWhoPayFee(DTOIntWhoPayFeeType whoPayFee) {
        this.whoPayFee = whoPayFee;
    }

}
