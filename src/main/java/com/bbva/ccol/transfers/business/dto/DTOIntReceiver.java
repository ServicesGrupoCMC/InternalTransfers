
package com.bbva.ccol.transfers.business.dto;




public class DTOIntReceiver {

    public final static long serialVersionUID = 1L;
    private String alias;
    private DTOIntContract contract;
    private DTOIntHolder holder;

    public DTOIntReceiver() {
        //default constructor
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public DTOIntContract getContract() {
        return contract;
    }

    public void setContract(DTOIntContract contract) {
        this.contract = contract;
    }

    public DTOIntHolder getHolder() {
        return holder;
    }

    public void setHolder(DTOIntHolder holder) {
        this.holder = holder;
    }

}
