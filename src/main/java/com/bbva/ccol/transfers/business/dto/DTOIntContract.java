
package com.bbva.ccol.transfers.business.dto;




public class DTOIntContract {

    public final static long serialVersionUID = 1L;
    private String contractId;
    private String number;
    private DTOIntContractNumberFormat numberType;
    private DTOIntContractType contractType;

    public DTOIntContract() {
        //default constructor
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public DTOIntContractNumberFormat getNumberType() {
        return numberType;
    }

    public void setNumberType(DTOIntContractNumberFormat numberType) {
        this.numberType = numberType;
    }

    public DTOIntContractType getContractType() {
        return contractType;
    }

    public void setContractType(DTOIntContractType contractType) {
        this.contractType = contractType;
    }

}
