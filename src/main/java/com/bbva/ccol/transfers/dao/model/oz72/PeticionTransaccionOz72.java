package com.bbva.ccol.transfers.dao.model.oz72;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * <p>Transacci&oacute;n <code>OZ72</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionOz72</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionOz72</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: OZ72CE00.CCT.TXT
 * OZ72Creacion Transferencia              OZ        OZ72-ECBVDKNPO OZ72CE00            OZ72  NS3000NNNNNN    SSTN    C   SNNSSNNN  NN                2015-05-29CE15505 2015-11-2709.55.39CICSDC112015-05-29-10.56.29.766588CE15505 0001-01-010001-01-01
 * FICHERO: OZ72CE00.FDF.TXT
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|01|00001|SIMULAT   |                 |A|001|0|O|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|02|00002|CONCEPT   |                 |A|040|0|O|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|03|00042|SECTNUM   |                 |A|020|0|R|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|04|00062|SCTNTID   |                 |A|020|0|R|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|05|00082|SCTNTNM   |                 |A|020|0|O|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|06|00102|SECONID   |                 |A|010|0|O|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|07|00112|SECONNM   |                 |A|010|0|O|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|08|00122|REALIAS   |                 |A|030|0|O|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|09|00152|RCCTNUM   |                 |A|020|0|R|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|10|00172|RCCNTID   |                 |A|020|0|R|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|11|00192|RCCNTNM   |                 |A|020|0|O|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|12|00212|RECTYID   |                 |A|010|0|R|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|13|00222|RECTYNM   |                 |A|010|0|O|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|14|00232|REHOLNM   |                 |A|060|0|R|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|15|00292|RCHODNM   |                 |A|017|0|R|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|16|00309|RHODTID   |                 |A|020|0|R|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|17|00329|RHODTNM   |                 |A|020|0|R|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|18|00349|RHOCTYP   |                 |A|010|0|O|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|19|00359|RHOCTVL   |                 |A|010|0|O|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|20|00369|SEMOAMO   |                 |N|015|0|R|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|21|00384|SEMOCUR   |                 |A|003|0|R|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|22|00387|DATE      |                 |A|026|0|O|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|23|00413|NOTFTY1   |                 |A|010|0|R|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|24|00423|NOTFNM1   |                 |A|010|0|O|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|25|00433|NOTFVL1   |                 |A|080|0|R|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|26|00513|NTSTID1   |                 |A|010|0|O|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|27|00523|NTSTNM1   |                 |A|010|0|O|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|28|00533|NOTFTY2   |                 |A|010|0|O|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|29|00543|NOTFNM2   |                 |A|010|0|O|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|30|00553|NOTFVL2   |                 |A|080|0|R|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|31|00633|NTSTID2   |                 |A|010|0|O|        |
 * OZ72CE00|E-FORMATO ENTRADA CONSULTA    |F|05|00034|32|00643|NTSTNM2   |                 |A|010|0|O|        |
 * FICHERO: OZ72CS01.FDF
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|01|00021|INTINID    |                |A|040|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|02|00021|CONCEPT    |                |A|020|2|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|03|00081|SECTNUM    |                |A|020|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|04|00101|SCTNTNM    |                |A|020|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|05|00111|SECONID    |                |A|010|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|06|00171|SECONNM    |                |A|010|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|07|00181|REALIAS    |                |A|030|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|08|00211|RCCTNUM    |                |A|020|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|09|00121|SCTNTID    |                |A|020|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|10|00231|RCCNTID    |                |A|020|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|11|00251|RCCNTNM    |                |A|020|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|12|00271|RECTYID    |                |A|010|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|13|00281|RECTYNM    |                |A|010|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|14|00291|REHOLNM    |                |A|060|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|15|00351|RHOCTYP    |                |A|010|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|16|00361|RHOCTVL    |                |A|010|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|17|00371|CHARAMO    |                |A|017|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|18|00388|CHARCUR    |                |A|003|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|19|00391|DATE       |                |A|026|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|20|00417|MODDATE    |                |A|026|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|21|00443|ACCDATE    |                |A|026|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|22|00469|TOFEEAM    |                |A|017|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|23|00486|TOFEECU    |                |A|003|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|24|00501|TOTAXAM    |                |A|017|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|25|00518|TOTAXCU    |                |A|003|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|26|00521|EXCBACU    |                |A|003|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|27|00524|EXCTGCU    |                |A|003|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|28|00527|EXRATDT    |                |A|026|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|29|00553|EXRFTVL    |                |A|017|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|30|00570|EXRFTRT    |                |A|006|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|31|00576|EXRFTTY    |                |A|010|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|32|00586|STATUID    |                |A|010|0|S|        |
 * OZ72CS01|S-SALIDA LISTADO DE CAMPOS    |X|04|00084|33|00596|STATUNM    |                |A|010|0|S|        |
 * FICHERO: OZ72CS02.FDF
 * OZ72CS02|S - SALIDA INTERNAL TRANSFERS |X|04|00084|01|00001|RCHODID    |ID NUMERO DOCUME|A|040|0|S|        |
 * OZ72CS02|S - SALIDA INTERNAL TRANSFERS |X|04|00084|02|00041|RCHODNM    |NUMERO DOCUM. ID|A|017|0|S|        |
 * OZ72CS02|S - SALIDA INTERNAL TRANSFERS |X|04|00084|03|00058|RHODTID    |TIPO DOCUMENT ID|A|020|0|S|        |
 * OZ72CS02|S - SALIDA INTERNAL TRANSFERS |X|04|00084|04|00078|RHODTNM    |DESC TIPO DOCUME|A|020|0|S|        |
 * FICHERO: OZ72CS03.FDF
 * OZ72CS03|S - SALIDA INTERNAL TRANSFERS |X|04|00084|01|00001|SEMOAMO    |MONTO TRANSFEREN|A|017|0|S|        |
 * OZ72CS03|S - SALIDA INTERNAL TRANSFERS |X|04|00084|02|00018|SEMOCUR    |MONEDA MONTO TRA|A|003|0|S|        |
 * OZ72CS03|S - SALIDA INTERNAL TRANSFERS |X|04|00084|03|00021|SEMOIMJ    |INDICA MONEDA PR|A|001|0|S|        |
 * FICHERO: OZ72CS04.FDF
 * OZ72CS04|S - SALIDA INTERNAL TRANSFERS |X|04|00084|01|00001|FEITMTY    |TIPO DE COSTO TR|A|025|0|S|        |
 * OZ72CS04|S - SALIDA INTERNAL TRANSFERS |X|04|00084|02|00026|FEITMNM    |DESC TIPO COSTO |A|025|0|S|        |
 * OZ72CS04|S - SALIDA INTERNAL TRANSFERS |X|04|00084|03|00031|FEITMAM    |MONTO DEL COSTO |A|017|0|S|        |
 * OZ72CS04|S - SALIDA INTERNAL TRANSFERS |X|04|00084|04|00048|FEITMCU    |MONEDA COSTO TRA|A|003|0|S|        |
 * OZ72CS04|S - SALIDA INTERNAL TRANSFERS |X|04|00084|05|00051|FEITMPR    |PORCENTAJE DEL C|A|015|0|S|        |
 * OZ72CS04|S - SALIDA INTERNAL TRANSFERS |X|04|00084|06|00066|FTWPFID    |ID QUIEN PAGA CO|A|020|0|S|        |
 * OZ72CS04|S - SALIDA INTERNAL TRANSFERS |X|04|00084|07|00086|FTWPFNM    |NM QUIEN PAGA CO|A|020|0|S|        |
 * FICHERO: OZ72CS05.FDF
 * OZ72CS05|S - SALIDA INTERNAL TRANSFERS |X|04|00084|01|00001|TXITMTY    |TIPO IMPUESTO TR|A|025|0|S|        |
 * OZ72CS05|S - SALIDA INTERNAL TRANSFERS |X|04|00084|02|00026|TXITMNM    |DESC TIPO IMPTO |A|025|0|S|        |
 * OZ72CS05|S - SALIDA INTERNAL TRANSFERS |X|04|00084|03|00031|TXITMAM    |MONTO DEL IMPTO |A|017|0|S|        |
 * OZ72CS05|S - SALIDA INTERNAL TRANSFERS |X|04|00084|04|00048|TXITMCU    |MONEDA IMPTO TRA|A|003|0|S|        |
 * OZ72CS05|S - SALIDA INTERNAL TRANSFERS |X|04|00084|05|00051|TXITMPR    |PORCENTAJE DEL I|A|015|0|S|        |
 * FICHERO: OZ72CS06.FDF
 * OZ72CS06|S - SALIDA INTERNAL TRANSFERS |X|04|00084|01|00001|REFERID    |ID DE LA REFEREN|A|020|0|S|        |
 * OZ72CS06|S - SALIDA INTERNAL TRANSFERS |X|04|00084|02|00021|REFERNM    |NM DE LA REFEREN|A|025|0|S|        |
 * OZ72CS06|S - SALIDA INTERNAL TRANSFERS |X|04|00084|03|00046|REFERVL    |VALOR REFERENCIA|A|015|0|S|        |
 * FICHERO: OZ72CS07.FDF
 * OZ72CS05|S - SALIDA INTERNAL TRANSFERS |X|04|00084|01|00001|TXITMTY    |TIPO IMPUESTO TR|A|025|0|S|        |
 * OZ72CS07|S - SALIDA INTERNAL TRANSFERS |X|04|00084|01|00001|NOTIFTY    |TIPO DE NOTIFICA|A|010|0|S|        |
 * OZ72CS07|S - SALIDA INTERNAL TRANSFERS |X|04|00084|02|00011|NOTIFNM    |DESCR. TIPO NOTI|A|010|0|S|        |
 * OZ72CS07|S - SALIDA INTERNAL TRANSFERS |X|04|00084|03|00021|NOTIFVL    |CEL O EMAIL NOTI|A|080|0|S|        |
 * OZ72CS07|S - SALIDA INTERNAL TRANSFERS |X|04|00084|04|00101|NOTSTID    |ID ESTADO NOTIFI|A|010|0|S|        |
 * OZ72CS07|S - SALIDA INTERNAL TRANSFERS |X|04|00084|05|00111|NOTSTNM    |NM ESTADO NOTIFI|A|010|0|S|        |
 * FICHERO: OZ72CS00.FDX
 * OZ72OZ72CS01 OZ72CS01 OZ72OZ72CS01                         CE30793 2016-06-30-15.55.06.557756CE30793 2016-01-07-15.55.06.557765
 * OZ72OZ72CS02 OZ72CS02 OZ72OZ72CS02                         CE30793 2016-06-30-15.55.06.557756CE30793 2016-01-07-15.55.06.557765
 * OZ72OZ72CS03 OZ72CS03 OZ72OZ72CS03                         CE30793 2016-06-30-15.55.06.557756CE30793 2016-01-07-15.55.06.557765
 * OZ72OZ72CS04 OZ72CS04 OZ72OZ72CS04                         CE30793 2016-06-30-15.55.06.557756CE30793 2016-01-07-15.55.06.557765
 * OZ72OZ72CS05 OZ72CS05 OZ72OZ72CS05                         CE30793 2016-06-30-15.55.06.557756CE30793 2016-01-07-15.55.06.557765
 * OZ72OZ72CS06 OZ72CS06 OZ72OZ72CS06                         CE30793 2016-06-30-15.55.06.557756CE30793 2016-01-07-15.55.06.557765
 * OZ72OZ72CS07 OZ72CS07 OZ72OZ72CS07                         CE30793 2016-06-30-15.55.06.557756CE30793 2016-01-07-15.55.06.557765
</pre></code>
 * 
 * @see RespuestaTransaccionOz72
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "OZ72",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionOz72.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoOZ72CE00.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionOz72 implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}