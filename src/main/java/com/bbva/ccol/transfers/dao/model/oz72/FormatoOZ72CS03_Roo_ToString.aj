// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.ccol.transfers.dao.model.oz72;

import java.lang.String;

privileged aspect FormatoOZ72CS03_Roo_ToString {
    
    public String FormatoOZ72CS03.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Semoamo: ").append(getSemoamo()).append(", ");
        sb.append("Semocur: ").append(getSemocur()).append(", ");
        sb.append("Semoimj: ").append(getSemoimj());
        return sb.toString();
    }
    
}
