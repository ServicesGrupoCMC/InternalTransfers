package com.bbva.ccol.transfers.dao;

import javax.annotation.Resource;

import com.bbva.ccol.transfers.business.dto.DTOIntInternalTransfer;
import com.bbva.ccol.transfers.facade.v01.dto.InternalTransfer;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.ibm.disthub2.impl.matching.selector.ParseException;

public interface TransfersDAO {

	//TODO: method signatures for DAO

	public DTOIntInternalTransfer createTransfer(InternalTransfer infoInternalTransfer)throws BusinessServiceException, ParseException;
	
	public DTOIntInternalTransfer createTransfer(DTOIntInternalTransfer dtoIntInternalTransfer, String simulat)  throws BusinessServiceException, ParseException, java.text.ParseException;


}

