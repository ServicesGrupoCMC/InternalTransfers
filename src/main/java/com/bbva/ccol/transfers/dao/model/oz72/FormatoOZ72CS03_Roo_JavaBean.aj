// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.ccol.transfers.dao.model.oz72;

import java.lang.String;

privileged aspect FormatoOZ72CS03_Roo_JavaBean {
    
    public String FormatoOZ72CS03.getSemoamo() {
        return this.semoamo;
    }
    
    public void FormatoOZ72CS03.setSemoamo(String semoamo) {
        this.semoamo = semoamo;
    }
    
    public String FormatoOZ72CS03.getSemocur() {
        return this.semocur;
    }
    
    public void FormatoOZ72CS03.setSemocur(String semocur) {
        this.semocur = semocur;
    }
    
    public String FormatoOZ72CS03.getSemoimj() {
        return this.semoimj;
    }
    
    public void FormatoOZ72CS03.setSemoimj(String semoimj) {
        this.semoimj = semoimj;
    }
    
}
