// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.ccol.transfers.dao.model.oz72;

import java.lang.String;

privileged aspect FormatoOZ72CS06_Roo_JavaBean {
    
    public String FormatoOZ72CS06.getReferid() {
        return this.referid;
    }
    
    public void FormatoOZ72CS06.setReferid(String referid) {
        this.referid = referid;
    }
    
    public String FormatoOZ72CS06.getRefernm() {
        return this.refernm;
    }
    
    public void FormatoOZ72CS06.setRefernm(String refernm) {
        this.refernm = refernm;
    }
    
    public String FormatoOZ72CS06.getRefervl() {
        return this.refervl;
    }
    
    public void FormatoOZ72CS06.setRefervl(String refervl) {
        this.refervl = refervl;
    }
    
}
