package com.bbva.ccol.transfers.dao.model.oz72;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;


/**
 * Formato de datos <code>OZ72CS04</code> de la transacci&oacute;n <code>OZ72</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "OZ72CS04")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoOZ72CS04 {
	
	/**
	 * <p>Campo <code>FEITMTY</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "FEITMTY", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
	private String feitmty;
	
	/**
	 * <p>Campo <code>FEITMNM</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "FEITMNM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
	private String feitmnm;
	
	/**
	 * <p>Campo <code>FEITMAM</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "FEITMAM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 17, longitudMaxima = 17)
	private String feitmam;
	
	/**
	 * <p>Campo <code>FEITMCU</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "FEITMCU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String feitmcu;
	
	/**
	 * <p>Campo <code>FEITMPR</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "FEITMPR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String feitmpr;
	
	/**
	 * <p>Campo <code>FTWPFID</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "FTWPFID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String ftwpfid;
	
	/**
	 * <p>Campo <code>FTWPFNM</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "FTWPFNM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String ftwpfnm;
	
}