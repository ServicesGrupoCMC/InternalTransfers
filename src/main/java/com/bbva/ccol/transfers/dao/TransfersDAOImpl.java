package com.bbva.ccol.transfers.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.ccol.transfers.business.dto.DTOIntInternalTransfer;
import com.bbva.ccol.transfers.facade.v01.dto.InternalTransfer;
import com.bbva.ccol.transfers.facade.v01.mapper.Mapper;
import com.bbva.ccol.transfers.dao.model.oz72.FormatoOZ72CE00;
import com.bbva.ccol.transfers.dao.model.oz72.FormatoOZ72CS01;
import com.bbva.ccol.transfers.dao.model.oz72.FormatoOZ72CS02;
import com.bbva.ccol.transfers.dao.model.oz72.FormatoOZ72CS03;
import com.bbva.ccol.transfers.dao.model.oz72.FormatoOZ72CS04;
import com.bbva.ccol.transfers.dao.model.oz72.FormatoOZ72CS05;
import com.bbva.ccol.transfers.dao.model.oz72.FormatoOZ72CS06;
import com.bbva.ccol.transfers.dao.model.oz72.FormatoOZ72CS07;
import com.bbva.ccol.transfers.dao.model.oz72.PeticionTransaccionOz72;
import com.bbva.ccol.transfers.dao.model.oz72.RespuestaTransaccionOz72;
import com.bbva.ccol.transfers.dao.model.oz72.TransaccionOz72;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.ErrorMappingHelper;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.ibm.disthub2.impl.matching.selector.ParseException;
import javax.annotation.Resource;

@Component(value = "transfersDaoImpl")

public class TransfersDAOImpl  implements TransfersDAO {
	
	
	  
	//TODO: method implementations
	@Autowired
	private TransaccionOz72 oz72;

	@Autowired
	private ErrorMappingHelper emh;


	@Override
	public DTOIntInternalTransfer createTransfer(DTOIntInternalTransfer dtoIntInternalTransfer, String simulat)throws BusinessServiceException, ParseException, java.text.ParseException {
		// TODO Auto-generated method stub

		PeticionTransaccionOz72 req = new PeticionTransaccionOz72();
		FormatoOZ72CE00 entrada;//se instancia el formato de entrada para mandarle los parametros 
		entrada = Mapper.entradaHost(dtoIntInternalTransfer,simulat );//aca se le envia el dto interno al mapper y en este caso se le esta quemando una "f" en lugar de simulate  en modo de prueba para ver si el servicio funciona (cambiandolo por el parametro de entrada se estaria recibiendo desde el front )
		req.getCuerpo().getPartes().add(entrada);//se a�ade a la transaccion (FUNCIONALIDAD?)
		
		
		RespuestaTransaccionOz72 res = oz72.invocar(req);
		BusinessServiceException exception = emh.toBusinessServiceException(res);
		if (exception != null) {
			throw exception;
		}
		
		
		
		CopySalida cs = res.getCuerpo().getParte(CopySalida.class);
		//En respuesta de la transaccion se le copia cada uno de los formatos de salida, averiguar con marlon si aca es donde hay que hacer las validaciones para la seleccion del formato 
		FormatoOZ72CS01 salida = cs.getCopy(FormatoOZ72CS01.class);
		FormatoOZ72CS02 salida2 = cs.getCopy(FormatoOZ72CS02.class);
		FormatoOZ72CS03 salida3 = cs.getCopy(FormatoOZ72CS03.class);
		FormatoOZ72CS04 salida4 = cs.getCopy(FormatoOZ72CS04.class);
		FormatoOZ72CS05 salida5 = cs.getCopy(FormatoOZ72CS05.class);
		FormatoOZ72CS06 salida6 = cs.getCopy(FormatoOZ72CS06.class);
		FormatoOZ72CS07 salida7 = cs.getCopy(FormatoOZ72CS07.class);
		
		/*
		salida2 = cs.getCopy(FormatoOZ72CS02.class);
		salida3 = cs.getCopy(FormatoOZ72CS03.class);
		salida4 = cs.getCopy(FormatoOZ72CS04.class);
		salida5 = cs.getCopy(FormatoOZ72CS05.class);
		salida6 = cs.getCopy(FormatoOZ72CS06.class);
		salida7 = cs.getCopy(FormatoOZ72CS07.class);
		*/
		//return Mapper.dtoIntInternalTS1(salida);
		return Mapper.salidaHost(salida);
	}
	
	
	//esta en la interfaz del DAO y se supone que aca se haria la salida ????????
	@Override
	public DTOIntInternalTransfer createTransfer(InternalTransfer infoInternalTransfer)throws BusinessServiceException, ParseException {
		// TODO Auto-generated method stub
		return null;
	}

}



