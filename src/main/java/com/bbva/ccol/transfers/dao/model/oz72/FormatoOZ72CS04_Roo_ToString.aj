// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.ccol.transfers.dao.model.oz72;

import java.lang.String;

privileged aspect FormatoOZ72CS04_Roo_ToString {
    
    public String FormatoOZ72CS04.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Feitmam: ").append(getFeitmam()).append(", ");
        sb.append("Feitmcu: ").append(getFeitmcu()).append(", ");
        sb.append("Feitmnm: ").append(getFeitmnm()).append(", ");
        sb.append("Feitmpr: ").append(getFeitmpr()).append(", ");
        sb.append("Feitmty: ").append(getFeitmty()).append(", ");
        sb.append("Ftwpfid: ").append(getFtwpfid()).append(", ");
        sb.append("Ftwpfnm: ").append(getFtwpfnm());
        return sb.toString();
    }
    
}
