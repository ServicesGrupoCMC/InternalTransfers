
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "contractType", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "contractType", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContractType
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Contract type identifier.", required = true)
    private String id;
    @ApiModelProperty(value = "Contract name.", required = true)
    private String name;

    public ContractType() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
