
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "whoPayFeeType", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "whoPayFeeType", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class WhoPayFeeType
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Who pay fees identifier.", required = true)
    private String id;
    @ApiModelProperty(value = "Who pay fees name.", required = true)
    private String name;

    public WhoPayFeeType() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
