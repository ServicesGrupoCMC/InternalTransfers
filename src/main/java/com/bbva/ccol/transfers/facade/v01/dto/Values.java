
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "values", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "values", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Values
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Factor associated to the exchange rate ", required = false)
    private Factor factor;
    @ApiModelProperty(value = "Notification type", required = false)
    private String type;

    public Values() {
        //default constructor
    }

    public Factor getFactor() {
        return factor;
    }

    public void setFactor(Factor factor) {
        this.factor = factor;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
