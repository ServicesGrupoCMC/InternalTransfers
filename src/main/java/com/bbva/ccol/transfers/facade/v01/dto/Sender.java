
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.wordnik.swagger.annotations.ApiModelProperty;



@XmlRootElement(name = "sender", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "sender", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Sender
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Sender of the internal transfer. Represents information about the origin of the transfer which, in the case of a internal transfer, could be only a product", required = true)
    private Contract contract;

    public Sender() {
        //default constructor
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

}
