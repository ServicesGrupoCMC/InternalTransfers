package com.bbva.ccol.transfers.facade.v01;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.bbva.ccol.transfers.facade.v01.dto.InternalTransfer;
import com.bbva.ccol.transfers.facade.v01.dto.ItemizeTaxes;
import com.bbva.ccol.transfers.facade.v01.dto.ChargeAmount;
import com.bbva.ccol.transfers.facade.v01.dto.IdentityDocument;
import com.bbva.ccol.transfers.facade.v01.dto.SentMoney;
import com.bbva.ccol.transfers.facade.v01.dto.Holder;
import com.bbva.ccol.transfers.facade.v01.dto.DocumentType;
import com.bbva.ccol.transfers.facade.v01.dto.Receiver;
import com.bbva.ccol.transfers.facade.v01.dto.Contract;
import com.bbva.ccol.transfers.facade.v01.dto.ExchangeRate;
import com.bbva.ccol.transfers.facade.v01.dto.Sender;
import com.bbva.ccol.transfers.facade.v01.dto.ItemizeFees;
import com.bbva.ccol.transfers.facade.v01.dto.WhoPayFeeType;
import com.bbva.ccol.transfers.facade.v01.dto.Amount;
import com.bbva.ccol.transfers.facade.v01.dto.InternalTransferFees;
import com.bbva.ccol.transfers.facade.v01.dto.References;
import com.bbva.ccol.transfers.facade.v01.dto.TotalFees;
import com.bbva.ccol.transfers.facade.v01.dto.ContractNumberFormat;
import com.bbva.ccol.transfers.facade.v01.dto.ContractType;
import com.bbva.ccol.transfers.facade.v01.dto.InternalTransferTaxes;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.ibm.disthub2.impl.matching.selector.ParseException;
import com.wordnik.swagger.annotations.ApiParam;


public interface ISrvTransfersV01 {
 	public Response createInternalTransfer(@ApiParam(value="Transfer to add") 	 @DefaultValue("null") @QueryParam("Simulated") String simulat,InternalTransfer infoInternalTransfer) 
 	throws BusinessServiceException, ParseException, java.text.ParseException;

	
}