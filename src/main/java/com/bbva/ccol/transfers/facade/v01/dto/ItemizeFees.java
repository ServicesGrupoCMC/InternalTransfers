
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "itemizeFees", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "itemizeFees", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemizeFees
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Identifier of the fee type.", required = true)
    private String feeType;
    @ApiModelProperty(value = "Description of the fee.", required = true)
    private String name;
    @ApiModelProperty(value = "Description of the fee.", required = true)
    private Amount amount;
    @ApiModelProperty(value = "Percentage of fee.", required = true)
    private Double percentage;
    @ApiModelProperty(value = "Indicates who pays the fee.", required = true)
    private WhoPayFeeType whoPayFee;

    public ItemizeFees() {
        //default constructor
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Amount getAmount() {
        return amount;
    }

    public void setAmount(Amount amount) {
        this.amount = amount;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }

    public WhoPayFeeType getWhoPayFee() {
        return whoPayFee;
    }

    public void setWhoPayFee(WhoPayFeeType whoPayFee) {
        this.whoPayFee = whoPayFee;
    }

}
