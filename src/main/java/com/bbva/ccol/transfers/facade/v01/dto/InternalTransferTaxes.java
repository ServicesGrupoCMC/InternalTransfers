
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "internalTransferTaxes", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "internalTransferTaxes", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class InternalTransferTaxes
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Sum of total amount of all taxes.", required = true)
    private Amount totalTaxes;
    @ApiModelProperty(value = "Sum of total amount of all taxes.", required = true)
    private List<ItemizeTaxes> itemizeTaxes;
    @ApiModelProperty(value = "Percentage of transaction tax.", required = true)
    private Double percentage;

    public InternalTransferTaxes() {
        //default constructor
    }

    public Amount getTotalTaxes() {
        return totalTaxes;
    }

    public void setTotalTaxes(Amount totalTaxes) {
        this.totalTaxes = totalTaxes;
    }

    public List<ItemizeTaxes> getItemizeTaxes() {
        return itemizeTaxes;
    }

    public void setItemizeTaxes(List<ItemizeTaxes> itemizeTaxes) {
        this.itemizeTaxes = itemizeTaxes;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }

}
