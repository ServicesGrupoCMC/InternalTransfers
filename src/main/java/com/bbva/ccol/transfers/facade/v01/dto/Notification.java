
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "notification", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "notification", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Notification
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "SMS: Short Message Service or EMAIL: Email as the communication channel by which the notification will be sent.", required = true)
    private String notificationType;
    @ApiModelProperty(value = "Notification name..", required = false)
    private String name;
    @ApiModelProperty(value = "Mobile number or email of the receiver used according to the notification type..", required = true)
    private String value;
    @ApiModelProperty(value = "Identity document type.", required = false)
    private Status status;

    public Notification() {
        //default constructor
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
