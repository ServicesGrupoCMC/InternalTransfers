
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "references", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "references", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class References
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Reference identifier.", required = true)
    private String id;
    @ApiModelProperty(value = "Reference name.", required = true)
    private String name;
    @ApiModelProperty(value = "Reference value.", required = true)
    private String value;

    public References() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
