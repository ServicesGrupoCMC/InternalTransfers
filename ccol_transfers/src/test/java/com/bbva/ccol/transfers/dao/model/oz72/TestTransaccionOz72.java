package com.bbva.ccol.transfers.dao.model.oz72;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ExcepcionRespuestaHost;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.ErrorMappingHelper;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.host.transporte.ExcepcionTransporte;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.jee.arq.spring.core.servicing.test.BusinessServiceTestContextLoader;
import com.bbva.jee.arq.spring.core.servicing.test.MockInvocationContextTestExecutionListener;

/**
 * Test de la transacci&oacute;n <code>OZ72</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
	loader = BusinessServiceTestContextLoader.class, 
	locations = {
        "classpath*:/META-INF/spring/applicationContext-*.xml", 
        "classpath:/META-INF/spring/business-service.xml",
        "classpath:/META-INF/spring/business-service-test.xml"
    }
)
@TestExecutionListeners(listeners = { MockInvocationContextTestExecutionListener.class, DependencyInjectionTestExecutionListener.class })
public class TestTransaccionOz72 {
	
	private static final Log LOG = LogFactory.getLog(TestTransaccionOz72.class);
		
	@Autowired
	private TransaccionOz72 transaccion;
	
	@Autowired
	private ErrorMappingHelper errorMappingHelper;
	

	
	@Test
	public void test() throws ExcepcionTransaccion {
		
		PeticionTransaccionOz72 peticion = new PeticionTransaccionOz72();
		FormatoOZ72CE00 formatoEntrada = new FormatoOZ72CE00();
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		formatoEntrada.setConcept("concept");
		
		peticion.getCuerpo().getPartes().add(formatoEntrada);		
		

		
		/*
		 * TODO: poblar la peticion con valores adecuados
		 */
		
		try {
			LOG.info("Invocando transaccion, peticion: " + peticion);
			RespuestaTransaccionOz72 respuesta = transaccion.invocar(peticion);
			LOG.info("Recibida respuesta: " + respuesta);
			
			
			BusinessServiceException exception = errorMappingHelper.toBusinessServiceException(respuesta);
			if ( exception != null ) throw exception;
			
			CopySalida outputCopy = respuesta.getCuerpo().getParte(CopySalida.class);
			
			
			
			if ( outputCopy != null ) {
				
				FormatoOZ72CE00 formatoSalida = outputCopy.getCopy(FormatoOZ72CE00.class);
				if ( formatoSalida != null ) {
					
					String mensaje = formatoSalida.getConcept();
					LOG.info("Recibido mensaje de respuesta: " + mensaje);
				}				
			}			
			


		} catch ( ExcepcionRespuestaHost e ) {
			LOG.error("Error recibido desde host, codigoError: " + e.getCodigoError() + ", descripcion: " + e.getMessage());
		} catch ( ExcepcionTransporte e ) {
			LOG.error("Error de transporte", e);
		}
	}
}