
package com.bbva.ccol.transfers.business.dto;




public class DTOIntSentMoney {

    public final static long serialVersionUID = 1L;
    private Double amount;
    private String currency;
    private Boolean isMajor;

    public DTOIntSentMoney() {
        //default constructor
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Boolean getIsMajor() {
        return isMajor;
    }

    public void setIsMajor(Boolean isMajor) {
        this.isMajor = isMajor;
    }

}
