
package com.bbva.ccol.transfers.business.dto;




public class DTOIntItemizeTaxes {

    public final static long serialVersionUID = 1L;
    private String taxType;
    private String name;
    private String percentage;
    private DTOIntAmount amount;

    public DTOIntItemizeTaxes() {
        //default constructor
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public DTOIntAmount getAmount() {
        return amount;
    }

    public void setAmount(DTOIntAmount amount) {
        this.amount = amount;
    }

}
