package com.bbva.ccol.transfers.dao.model.oz72;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>OZ72</code>
 * 
 * @see PeticionTransaccionOz72
 * @see RespuestaTransaccionOz72
 */
@Component
public class TransaccionOz72 implements InvocadorTransaccion<PeticionTransaccionOz72,RespuestaTransaccionOz72> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionOz72 invocar(PeticionTransaccionOz72 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionOz72.class, RespuestaTransaccionOz72.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionOz72 invocarCache(PeticionTransaccionOz72 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionOz72.class, RespuestaTransaccionOz72.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}