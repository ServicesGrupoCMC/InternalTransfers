
package com.bbva.ccol.transfers.business.dto;




public class DTOIntValues {

    public final static long serialVersionUID = 1L;
    private DTOIntFactor factor;
    private String type;

    public DTOIntValues() {
        //default constructor
    }

    public DTOIntFactor getFactor() {
        return factor;
    }

    public void setFactor(DTOIntFactor factor) {
        this.factor = factor;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
