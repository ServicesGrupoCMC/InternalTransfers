
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "chargeAmount", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "chargeAmount", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChargeAmount
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Charge monetary amount.", required = true)
    private Double amount;
    @ApiModelProperty(value = "String based on ISO-4217 for specifying the currency related to the charge monetary amount.", required = true)
    private String currency;

    public ChargeAmount() {
        //default constructor
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
