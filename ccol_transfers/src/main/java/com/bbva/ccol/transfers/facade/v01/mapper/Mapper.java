package com.bbva.ccol.transfers.facade.v01.mapper;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.BeanUtils;

import com.bbva.ccol.commons.rm.utils.encrypt.EncryptAes; 
import com.bbva.ccol.transfers.facade.v01.SrvTransfersV01;
import com.bbva.ccol.transfers.facade.v01.dto.InternalTransfer;
import com.bbva.ccol.transfers.facade.v01.dto.ItemizeTaxes;
import com.bbva.ccol.transfers.facade.v01.dto.ChargeAmount;
import com.bbva.ccol.transfers.facade.v01.dto.IdentityDocument;
import com.bbva.ccol.transfers.facade.v01.dto.SentMoney;
import com.bbva.ccol.transfers.facade.v01.dto.Holder;
import com.bbva.ccol.transfers.facade.v01.dto.DocumentType;
import com.bbva.ccol.transfers.facade.v01.dto.Receiver;
import com.bbva.ccol.transfers.facade.v01.dto.Status;
import com.bbva.ccol.transfers.facade.v01.dto.Contract;
import com.bbva.ccol.transfers.facade.v01.dto.ExchangeRate;
import com.bbva.ccol.transfers.facade.v01.dto.Factor;
import com.bbva.ccol.transfers.facade.v01.dto.Sender;
import com.bbva.ccol.transfers.facade.v01.dto.Amount;
import com.bbva.ccol.transfers.facade.v01.dto.WhoPayFeeType;
import com.bbva.ccol.transfers.facade.v01.dto.ItemizeFees;
import com.bbva.ccol.transfers.facade.v01.dto.InternalTransferFees;
import com.bbva.ccol.transfers.facade.v01.dto.References;
import com.bbva.ccol.transfers.facade.v01.dto.Values;
import com.bbva.ccol.transfers.facade.v01.dto.TotalFees;
import com.bbva.ccol.transfers.facade.v01.dto.ContractNumberFormat;
import com.bbva.ccol.transfers.facade.v01.dto.ContractType;
import com.bbva.ccol.transfers.facade.v01.dto.Notification;
import com.bbva.ccol.transfers.facade.v01.dto.InternalTransferTaxes;
import com.bbva.ccol.transfers.business.dto.DTOIntInternalTransfer;
import com.bbva.ccol.transfers.business.dto.DTOIntItemizeTaxes;
import com.bbva.ccol.transfers.business.dto.DTOIntChargeAmount;
import com.bbva.ccol.transfers.business.dto.DTOIntIdentityDocument;
import com.bbva.ccol.transfers.business.dto.DTOIntSentMoney;
import com.bbva.ccol.transfers.business.dto.DTOIntHolder;
import com.bbva.ccol.transfers.business.dto.DTOIntDocumentType;
import com.bbva.ccol.transfers.business.dto.DTOIntReceiver;
import com.bbva.ccol.transfers.business.dto.DTOIntStatus;
import com.bbva.ccol.transfers.business.dto.DTOIntContract;
import com.bbva.ccol.transfers.business.dto.DTOIntExchangeRate;
import com.bbva.ccol.transfers.business.dto.DTOIntFactor;
import com.bbva.ccol.transfers.business.dto.DTOIntSender;
import com.bbva.ccol.transfers.business.dto.DTOIntAmount;
import com.bbva.ccol.transfers.business.dto.DTOIntWhoPayFeeType;
import com.bbva.ccol.transfers.business.dto.DTOIntItemizeFees;
import com.bbva.ccol.transfers.business.dto.DTOIntInternalTransferFees;
import com.bbva.ccol.transfers.business.dto.DTOIntReferences;
import com.bbva.ccol.transfers.business.dto.DTOIntValues;
import com.bbva.ccol.transfers.business.dto.DTOIntTotalFees;
import com.bbva.ccol.transfers.business.dto.DTOIntContractNumberFormat;
import com.bbva.ccol.transfers.business.dto.DTOIntContractType;
import com.bbva.ccol.transfers.business.dto.DTOIntNotification;
import com.bbva.ccol.transfers.business.dto.DTOIntInternalTransferTaxes;


import com.bbva.ccol.transfers.dao.model.oz72.FormatoOZ72CE00;

import com.bbva.ccol.transfers.dao.model.oz72.FormatoOZ72CS01;
import com.bbva.ccol.transfers.dao.model.oz72.FormatoOZ72CS02;
import com.bbva.ccol.transfers.dao.model.oz72.FormatoOZ72CS03;
import com.bbva.ccol.transfers.dao.model.oz72.FormatoOZ72CS04;
import com.bbva.ccol.transfers.dao.model.oz72.FormatoOZ72CS05;
import com.bbva.ccol.transfers.dao.model.oz72.FormatoOZ72CS06;
import com.bbva.ccol.transfers.dao.model.oz72.FormatoOZ72CS07;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;




public class Mapper {
	
	private static I18nLog log = I18nLogFactory.getLogI18n(Mapper.class,"META-INF/spring/i18n/log/mensajesLog");
		
	
	
	private static Cipher obtieneCipher(boolean paraCifrar) throws Exception {
		final String llave = "1111111111111111";
		final SecretKeySpec key = new SecretKeySpec(llave.getBytes(), 0, 16, "AES");
		final Cipher aes = Cipher.getInstance("AES/ECB/PKCS5Padding");
		if (paraCifrar) {
		aes.init(Cipher.ENCRYPT_MODE, key);
		} else {
		aes.init(Cipher.DECRYPT_MODE, key);
		}

		return aes;
		}
	
	

	public static String decrypt(String encrypt){
		String decrypt = "";
		try {
			final byte[] encryptByte = Hex.decodeHex(encrypt.toCharArray());
			final Cipher aes = obtieneCipher(false);
			final byte[] bytes = aes.doFinal(encryptByte);
			decrypt = new String(bytes, "UTF-8");
		} catch (DecoderException e) {
			return null;
		} catch (Exception e) {
			return null;
		}
		return decrypt;
	}
	
	
	public static String encrypt(String stringIn) {

		String encryptString = "";
		try {
			final byte[] bytes = stringIn.getBytes("UTF-8");
			final Cipher aes = obtieneCipher(true);
			final byte[] encrypt = aes.doFinal(bytes);
			encryptString = Hex.encodeHexString(encrypt);
		} catch (UnsupportedEncodingException e) {
			return null;
		} catch (Exception e) {
			return null;
		}

		return encryptString;
	}

	
	
	
	
	public static FormatoOZ72CE00 entradaHost(DTOIntInternalTransfer dtoIn, String simulated){
		FormatoOZ72CE00 formatoOZ72CE00 = new FormatoOZ72CE00();
		
		//GRUPO CMC AJUSTE SIMALATE TIPO STRING INI
		if(dtoIn != null) {
			String isSimulated ;
			
			if (simulated != null){
				if (simulated=="S"){
					isSimulated = "S";
				}
				else{
					isSimulated = "N";
				}
				formatoOZ72CE00.setSimulat(isSimulated);
			}
			//GRUPO CMC AJUSTE SIMALATE TIPO STRING FIN
			
			formatoOZ72CE00.setConcept(dtoIn.getConcept());
			
			
			String cuentasender = null;
			if(dtoIn.getSender() != null) {
				if(dtoIn.getSender().getContract() != null) {
					
					cuentasender=encrypt(dtoIn.getSender().getContract().getNumber());
					formatoOZ72CE00.setSectnum(decrypt(cuentasender));

					if(dtoIn.getSender().getContract().getNumberType() != null) {
						formatoOZ72CE00.setSctntid(dtoIn.getSender().getContract().getNumberType().getId());
						formatoOZ72CE00.setSctntnm(dtoIn.getSender().getContract().getNumberType().getName());
					}

					if(dtoIn.getSender().getContract().getContractType() != null) {
						formatoOZ72CE00.setSeconid(dtoIn.getSender().getContract().getContractType().getId());
						formatoOZ72CE00.setSeconnm(dtoIn.getSender().getContract().getContractType().getName());
					}
				}
			}
			String cuentaReceiver=null;
			if(dtoIn.getReceiver() != null) {

				formatoOZ72CE00.setRealias((dtoIn.getReceiver().getAlias()));
				if (dtoIn.getReceiver().getContract() != null) {
					cuentaReceiver=encrypt(dtoIn.getReceiver().getContract().getNumber());
					formatoOZ72CE00.setRcctnum(decrypt(cuentaReceiver));
					if (dtoIn.getReceiver().getContract().getNumberType() != null) {
						formatoOZ72CE00.setRccntid(dtoIn.getReceiver().getContract().getNumberType().getId());
						formatoOZ72CE00.setRccntnm(dtoIn.getReceiver().getContract().getNumberType().getName());
					}

					if (dtoIn.getReceiver().getContract().getContractType() != null) {
						formatoOZ72CE00.setRectyid(dtoIn.getReceiver().getContract().getContractType().getId());
						formatoOZ72CE00.setRectynm(dtoIn.getReceiver().getContract().getContractType().getName());
					}
				}

				//GRUPO CMC AJUSTE HOLDER NULO INI
				if(dtoIn.getReceiver().getHolder() != null){
					formatoOZ72CE00.setReholnm(dtoIn.getReceiver().getHolder().getHolderName());
					
					for (DTOIntIdentityDocument data : dtoIn.getReceiver().getHolder().getIdentityDocuments()) {

						formatoOZ72CE00.setRhodtnm(data.getDocumentNumber());
						formatoOZ72CE00.setRhodtid(data.getDocumentType().getId());
						formatoOZ72CE00.setRhodtnm(data.getDocumentType().getName());
					}

					formatoOZ72CE00.setRhoctyp(dtoIn.getReceiver().getHolder().getContactType());
					formatoOZ72CE00.setRhoctvl(dtoIn.getReceiver().getHolder().getContactValue());
					
					
				}
				//GRUPO CMC AJUSTE HOLDER NULO FIN
				
				for (DTOIntSentMoney date : dtoIn.getSentMoney()) {
					
					//GRUPO CMC AJUSTE OBJETO INEXISTENTE EN SENT MONEY INI
					
					formatoOZ72CE00.setSemoamo(date.getAmount().longValue());
					formatoOZ72CE00.setSemocur(date.getCurrency());
					
					//GRUPO CMC AJUSTE OBJETO INEXISTENTE EN SENT MONEY FIN
				}
			}

			if(dtoIn.getDate() != null){
				formatoOZ72CE00.setDate((dtoIn.getDate()));
			}

			if (dtoIn.getNotification() != null) {
				for (int i = 0; i < dtoIn.getNotification().size(); i++) {
					if (i == 0) {
						formatoOZ72CE00.setNotfty1(dtoIn.getNotification().get(i).getNotificationType());
						formatoOZ72CE00.setNotfnm1(dtoIn.getNotification().get(i).getName());
						formatoOZ72CE00.setNotfvl1(dtoIn.getNotification().get(i).getValue());
						formatoOZ72CE00.setNtstid1(dtoIn.getNotification().get(i).getStatus().getId());
						formatoOZ72CE00.setNtstnm1(dtoIn.getNotification().get(i).getStatus().getName());
					} else if (i == 1) {
						formatoOZ72CE00.setNotfty2(dtoIn.getNotification().get(i).getNotificationType());
						formatoOZ72CE00.setNotfnm2(dtoIn.getNotification().get(i).getName());
						formatoOZ72CE00.setNotfvl2(dtoIn.getNotification().get(i).getValue());
						formatoOZ72CE00.setNtstid2(dtoIn.getNotification().get(i).getStatus().getId());
						formatoOZ72CE00.setNtstnm2(dtoIn.getNotification().get(i).getStatus().getName());
					}
				}
			}
		}

		return formatoOZ72CE00;
	}
	
	public static DTOIntInternalTransfer dtoIntInternalTS1(FormatoOZ72CS01 salida01Host) throws ParseException {
		
		//INSTANCIAS
		DTOIntInternalTransfer response1 = new DTOIntInternalTransfer();
		DTOIntAmount monto =new DTOIntAmount();
		DTOIntExchangeRate tasaDeCambio =new DTOIntExchangeRate();
		DTOIntFactor factores_TasaDeCambio =new DTOIntFactor();
		DTOIntValues valores_TasaDeCambio =new DTOIntValues();
		DTOIntContractNumberFormat formatoNumeroContrato  =new DTOIntContractNumberFormat();
		DTOIntContract contrato =new DTOIntContract();
		DTOIntReceiver recividor =new DTOIntReceiver();
	    DTOIntContractType tipoContrato  =new DTOIntContractType();
	    DTOIntHolder titular =new DTOIntHolder();
	    DTOIntTotalFees pagosTotales =new DTOIntTotalFees();
	    DTOIntStatus estados =new DTOIntStatus();
	    
	    
	    //MAPEOS
	    
		response1.setDate(DateFormat.getTimeInstance().parse(salida01Host.getDate()));
		if (salida01Host.getAccdate() != null){
			response1.setAccountingDate(DateFormat.getDateInstance().parse(salida01Host.getAccdate()));
		}
		monto.setAmount(Double.valueOf(salida01Host.getCharamo()));
		monto.setCurrency(String.valueOf(salida01Host.getCharcur()));
		if (salida01Host.getConcept() != null){
			response1.setConcept(String.valueOf(salida01Host.getConcept()));	
		}
		response1.setConcept(String.valueOf(salida01Host.getConcept()));
		if (salida01Host.getExcbacu() != null){
			tasaDeCambio.setBaseCurrency(String.valueOf(salida01Host.getExcbacu()));
		}
		if (salida01Host.getExctgcu() != null){
			tasaDeCambio.setTargetCurrency(String.valueOf(salida01Host.getExctgcu()));
		}
		if (salida01Host.getExratdt() != null){
			tasaDeCambio.setDate(DateFormat.getDateInstance().parse(salida01Host.getExratdt()));
		}
		if (salida01Host.getExrftrt() != null){
			factores_TasaDeCambio.setRatio(Double.valueOf(salida01Host.getExrftrt()));
		}
		if (salida01Host.getExrftvl() != null){
			factores_TasaDeCambio.setValue(Double.valueOf(salida01Host.getExrftvl()));
		}
		if (salida01Host.getExrftty() != null){
			valores_TasaDeCambio.setType(String.valueOf(salida01Host.getExrftty()));
		}
		
		response1.setInternalTransferId(String.valueOf(salida01Host.getIntinid()));
		if (salida01Host.getRccntid() != null){
			formatoNumeroContrato.setId(String.valueOf(salida01Host.getRccntid()));
		}
		if (salida01Host.getRccntnm() != null){
			formatoNumeroContrato.setName(String.valueOf(salida01Host.getRccntnm()));
		}
		
		contrato.setNumber(String.valueOf(salida01Host.getRcctnum()));
		if (salida01Host.getModdate() != null){
			response1.setModifiedDate(DateFormat.getDateInstance().parse(salida01Host.getModdate()));
		}
		if (salida01Host.getRealias() != null){
			recividor.setAlias(String.valueOf(salida01Host.getRealias()));
		}
		if (salida01Host.getRealias() != null){
			tipoContrato.setId(String.valueOf(salida01Host.getRectyid()));
		}
		if (salida01Host.getRectynm() != null){
			tipoContrato.setName(String.valueOf(salida01Host.getRectynm()));
		}
		if (salida01Host.getReholnm() != null){
			titular.setHolderName(String.valueOf(salida01Host.getReholnm()));
		}
		if (salida01Host.getRhoctvl() != null){
			titular.setContactValue(String.valueOf(salida01Host.getRhoctvl()));
		}
		if (salida01Host.getRhoctyp() != null){
			titular.setContactType(String.valueOf(salida01Host.getRhoctyp()));
		}
		pagosTotales.setAmount(Double.valueOf(salida01Host.getTofeeam()));
		pagosTotales.setCurrency(String.valueOf(salida01Host.getTofeecu()));
		
		if (salida01Host.getSctntid() != null){
			formatoNumeroContrato.setId(String.valueOf(salida01Host.getSctntid())); 
		}
		if (salida01Host.getSctntnm() != null){
			formatoNumeroContrato.setName(String.valueOf(salida01Host.getSctntnm()));
		}
		if (salida01Host.getSeconid() != null){
			tipoContrato.setId(String.valueOf(salida01Host.getSeconid()));
		}
		if (salida01Host.getSeconnm() != null){
			tipoContrato.setName(String.valueOf(salida01Host.getSeconnm()));
		}
		contrato.setNumber(String.valueOf(salida01Host.getSectnum()));
		
		monto.setAmount(Double.valueOf(salida01Host.getTotaxam()));
		monto.setCurrency(String.valueOf(salida01Host.getTotaxcu()));
		estados.setId(String.valueOf(salida01Host.getStatuid()));
		estados.setName(String.valueOf(salida01Host.getStatunm()));
		
		return response1;
	}	
	
public DTOIntInternalTransfer dtoIntInternalTS2(FormatoOZ72CS02 salida02Host) {
		
		DTOIntInternalTransfer response2 = new DTOIntInternalTransfer(); // MIRAR ACA COMO SE LE VUELVE A SETEAR LA INFORMACION EL INTERNAL TRANSFER
		
		DTOIntIdentityDocument documentoIdentificacion = new DTOIntIdentityDocument();
		DTOIntDocumentType tipoDocumento = new DTOIntDocumentType();
		
		documentoIdentificacion.setId(String.valueOf(salida02Host.getRchodid()));
		documentoIdentificacion.setDocumentNumber(String.valueOf(salida02Host.getRchodnm()));
		if (salida02Host.getRhodtid() != null){
			tipoDocumento.setId(String.valueOf(salida02Host.getRhodtid()));
		}
		if (salida02Host.getRhodtnm() != null){
			tipoDocumento.setName(String.valueOf(salida02Host.getRhodtnm()));
		}
		

		return response2;
	}
	
	public DTOIntInternalTransfer dtoIntInternalTS3(FormatoOZ72CS03 salida03Host) {
		DTOIntInternalTransfer response3 = new DTOIntInternalTransfer();
		DTOIntSentMoney dineroEnviado = new DTOIntSentMoney();
		
		dineroEnviado.setAmount(Double.valueOf(salida03Host.getSemoamo()));
		dineroEnviado.setCurrency(String.valueOf(salida03Host.getSemocur()));
		dineroEnviado.setIsMajor(Boolean.valueOf(salida03Host.getSemoimj()));

		return response3;
	}
	
	public DTOIntInternalTransfer dtoIntInternalTS4(FormatoOZ72CS04 salida04Host) {
		DTOIntInternalTransfer response4 = new DTOIntInternalTransfer();
		DTOIntItemizeFees detalleDePagos = new DTOIntItemizeFees();
		DTOIntAmount monto = new DTOIntAmount();
		DTOIntWhoPayFeeType quienRealizaElPago = new DTOIntWhoPayFeeType();
		
		monto.setAmount(Double.valueOf(salida04Host.getFeitmam()));
		monto.setCurrency(String.valueOf(salida04Host.getFeitmcu()));
		detalleDePagos.setName(String.valueOf(salida04Host.getFeitmnm()));
		if (salida04Host.getFeitmpr() != null){
			detalleDePagos.setPercentage(Double.valueOf(salida04Host.getFeitmpr()));
		}
		detalleDePagos.setFeeType(String.valueOf(salida04Host.getFeitmty()));
		if (salida04Host.getFtwpfid() != null){
			quienRealizaElPago.setId(String.valueOf(salida04Host.getFtwpfid()));
		}
		if (salida04Host.getFtwpfnm() != null){
			quienRealizaElPago.setName(String.valueOf(salida04Host.getFtwpfnm()));
		}


		return response4;
	}
	
	public DTOIntInternalTransfer dtoIntInternalTS5(FormatoOZ72CS05 salida05Host) {
		DTOIntInternalTransfer response5 = new DTOIntInternalTransfer();
		DTOIntAmount monto = new DTOIntAmount();
		DTOIntItemizeTaxes detalleDeImpuestos = new DTOIntItemizeTaxes();
		
		monto.setAmount(Double.valueOf(salida05Host.getTxitmam()));
		monto.setCurrency(String.valueOf(salida05Host.getTxitmcu()));
		detalleDeImpuestos.setName(String.valueOf(salida05Host.getTxitmnm()));
		if (salida05Host.getTxitmpr() != null){
			detalleDeImpuestos.setPercentage(String.valueOf(salida05Host.getTxitmpr())); 
		}
		detalleDeImpuestos.setTaxType(String.valueOf(salida05Host.getTxitmty()));
		
		return response5;
	}
	
	public DTOIntInternalTransfer dtoIntInternalTS6(FormatoOZ72CS06 salida06Host) {
		DTOIntInternalTransfer response6 = new DTOIntInternalTransfer();
		DTOIntReferences referencias = new DTOIntReferences();
		if (salida06Host.getReferid() != null){
			referencias.setId(String.valueOf(salida06Host.getReferid()));
		}
		if (salida06Host.getRefernm() != null){
			referencias.setName(String.valueOf(salida06Host.getRefernm()));
		}
		if (salida06Host.getRefervl() != null){
			referencias.setValue(String.valueOf(salida06Host.getRefervl()));
		}
		
		

		return response6;
	}
	
	public DTOIntInternalTransfer dtoIntInternalTS7(FormatoOZ72CS07 salida07Host) {
		
		DTOIntInternalTransfer response7 = new DTOIntInternalTransfer();
		DTOIntNotification notificaciones  = new DTOIntNotification();
		DTOIntStatus estado = new DTOIntStatus();
		if (salida07Host.getNotifnm() != null){
			notificaciones.setName(String.valueOf(salida07Host.getNotifnm()));
		}
		if (salida07Host.getNotifty() != null){
			notificaciones.setNotificationType(String.valueOf(salida07Host.getNotifty()));
		}
		if (salida07Host.getNotifvl() != null){
			notificaciones.setValue(String.valueOf(salida07Host.getNotifvl()));
		}
		if (salida07Host.getNotstid() != null){
			estado.setId(String.valueOf(salida07Host.getNotstid()));
		}
		if (salida07Host.getNotstnm() != null){
			estado.setName(String.valueOf(salida07Host.getNotstnm()));
		}


		return response7;
	}



	public static DTOIntInternalTransfer dtoIntInternalTransfer(InternalTransfer infoInternalTransfer) {
		
		
		DTOIntInternalTransfer dtoIntInternalTransfer = new DTOIntInternalTransfer(); 
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			String jsonInString = mapper.writeValueAsString(infoInternalTransfer);
			dtoIntInternalTransfer = new ObjectMapper().readValue(jsonInString, DTOIntInternalTransfer.class);
		} catch (Exception e) {
			log.debug("Error infoInternalTransfer to DTOIntInternalTransfer");
		}
		
 		return dtoIntInternalTransfer; 		
	}


	 public static InternalTransfer internalTransferMap(DTOIntInternalTransfer dtoIntInternalTransfer) { 
	 		InternalTransfer internalTransfer = new InternalTransfer(); 
	 		BeanUtils.copyProperties(dtoIntInternalTransfer, internalTransfer); 
	 		
	 		
	 		return internalTransfer; 
	 }



	public static DTOIntInternalTransfer salidaHost(FormatoOZ72CS01 salida) {
		// TODO Auto-generated method stub
		return null;
	}
	 		
	 		

 }