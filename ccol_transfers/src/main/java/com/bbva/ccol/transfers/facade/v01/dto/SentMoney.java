
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "sentMoney", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "sentMoney", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class SentMoney
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Transfer monetary amount.", required = true)
    private Double amount;
    @ApiModelProperty(value = "String based on ISO-4217 for specifying the currency related to the transfer monetary amount.", required = true)
    private String currency;
    @ApiModelProperty(value = "Flag indicating whether this currency corresponds to the main currency.", required = true)
    private Boolean isMajor;

    public SentMoney() {
        //default constructor
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Boolean getIsMajor() {
        return isMajor;
    }

    public void setIsMajor(Boolean isMajor) {
        this.isMajor = isMajor;
    }

}
