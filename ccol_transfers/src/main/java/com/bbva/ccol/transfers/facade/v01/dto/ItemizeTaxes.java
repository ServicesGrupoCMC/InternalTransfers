
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "itemizeTaxes", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "itemizeTaxes", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemizeTaxes
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Identifier of the tax type.", required = false)
    private String taxType;
    @ApiModelProperty(value = "Description of the tax type.", required = false)
    private String name;
    @ApiModelProperty(value = "percentage of transaction tax", required = false)
    private String percentage;
    @ApiModelProperty(value = "Currency value and associated tax the operation to perform.", required = false)
    private Amount amount;

    public ItemizeTaxes() {
        //default constructor
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public Amount getAmount() {
        return amount;
    }

    public void setAmount(Amount amount) {
        this.amount = amount;
    }

}
