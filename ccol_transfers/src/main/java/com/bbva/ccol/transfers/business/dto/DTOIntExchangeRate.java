
package com.bbva.ccol.transfers.business.dto;

import java.util.Date;



public class DTOIntExchangeRate {

    public final static long serialVersionUID = 1L;
    private String baseCurrency;
    private String targetCurrency;
    private Date date;
    private DTOIntValues values;

    public DTOIntExchangeRate() {
        //default constructor
    }

    public String getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public String getTargetCurrency() {
        return targetCurrency;
    }

    public void setTargetCurrency(String targetCurrency) {
        this.targetCurrency = targetCurrency;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public DTOIntValues getValues() {
        return values;
    }

    public void setValues(DTOIntValues values) {
        this.values = values;
    }

}
