package com.bbva.ccol.transfers.dao.model.oz72;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;


/**
 * Formato de datos <code>OZ72CS02</code> de la transacci&oacute;n <code>OZ72</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "OZ72CS02")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoOZ72CS02 {
	
	/**
	 * <p>Campo <code>RCHODID</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "RCHODID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
	private String rchodid;
	
	/**
	 * <p>Campo <code>RCHODNM</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "RCHODNM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 17, longitudMaxima = 17)
	private String rchodnm;
	
	/**
	 * <p>Campo <code>RHODTID</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "RHODTID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String rhodtid;
	
	/**
	 * <p>Campo <code>RHODTNM</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "RHODTNM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String rhodtnm;
	
}