
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "contract", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "contract", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Contract
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Corresponds with the identifier of a contract. It should be a code that identifies univocally a contract. ", required = true)
    private String contractId;
    @ApiModelProperty(value = "Sender of the internal transfer. Represents information about the origin of the transfer which, in the case of a internal transfer, could be only a product", required = true)
    private String number;
    @ApiModelProperty(value = "Sender of the internal transfer. Represents information about the origin of the transfer which, in the case of a internal transfer, could be only a product", required = true)
    private ContractNumberFormat numberType;
    @ApiModelProperty(value = "Sender of the internal transfer. Represents information about the origin of the transfer which, in the case of a internal transfer, could be only a product", required = true)
    private ContractType contractType;

    public Contract() {
        //default constructor
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public ContractNumberFormat getNumberType() {
        return numberType;
    }

    public void setNumberType(ContractNumberFormat numberType) {
        this.numberType = numberType;
    }

    public ContractType getContractType() {
        return contractType;
    }

    public void setContractType(ContractType contractType) {
        this.contractType = contractType;
    }

}
