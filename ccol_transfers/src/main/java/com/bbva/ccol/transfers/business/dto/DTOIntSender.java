
package com.bbva.ccol.transfers.business.dto;




public class DTOIntSender {

    public final static long serialVersionUID = 1L;
    private DTOIntContract contract;

    public DTOIntSender() {
        //default constructor
    }

    public DTOIntContract getContract() {
        return contract;
    }

    public void setContract(DTOIntContract contract) {
        this.contract = contract;
    }

}
