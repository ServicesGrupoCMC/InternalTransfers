
package com.bbva.ccol.transfers.business.dto;




public class DTOIntIdentityDocument {

    public final static long serialVersionUID = 1L;
    private String id;
    private String documentNumber;
    private DTOIntDocumentType documentType;

    public DTOIntIdentityDocument() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public DTOIntDocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DTOIntDocumentType documentType) {
        this.documentType = documentType;
    }

}
