
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "internalTransferFees", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "internalTransferFees", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class InternalTransferFees
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "", required = true)
    private TotalFees totalFees;
    @ApiModelProperty(value = "", required = true)
    private List<ItemizeFees> itemizeFees;

    public InternalTransferFees() {
        //default constructor
    }

    public TotalFees getTotalFees() {
        return totalFees;
    }

    public void setTotalFees(TotalFees totalFees) {
        this.totalFees = totalFees;
    }

    public List<ItemizeFees> getItemizeFees() {
        return itemizeFees;
    }

    public void setItemizeFees(List<ItemizeFees> itemizeFees) {
        this.itemizeFees = itemizeFees;
    }

}
