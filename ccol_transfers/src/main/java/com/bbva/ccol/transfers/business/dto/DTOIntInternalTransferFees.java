
package com.bbva.ccol.transfers.business.dto;

import java.util.List;



public class DTOIntInternalTransferFees {

    public final static long serialVersionUID = 1L;
    private DTOIntTotalFees totalFees;
    private List<DTOIntItemizeFees> itemizeFees;

    public DTOIntInternalTransferFees() {
        //default constructor
    }

    public DTOIntTotalFees getTotalFees() {
        return totalFees;
    }

    public void setTotalFees(DTOIntTotalFees totalFees) {
        this.totalFees = totalFees;
    }

    public List<DTOIntItemizeFees> getItemizeFees() {
        return itemizeFees;
    }

    public void setItemizeFees(List<DTOIntItemizeFees> itemizeFees) {
        this.itemizeFees = itemizeFees;
    }

}
