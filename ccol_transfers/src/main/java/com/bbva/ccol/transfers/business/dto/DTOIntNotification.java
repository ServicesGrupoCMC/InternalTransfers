
package com.bbva.ccol.transfers.business.dto;




public class DTOIntNotification {

    public final static long serialVersionUID = 1L;
    private String notificationType;
    private String name;
    private String value;
    private DTOIntStatus status;

    public DTOIntNotification() {
        //default constructor
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public DTOIntStatus getStatus() {
        return status;
    }

    public void setStatus(DTOIntStatus status) {
        this.status = status;
    }

}
