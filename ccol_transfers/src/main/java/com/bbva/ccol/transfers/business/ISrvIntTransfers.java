package com.bbva.ccol.transfers.business;

import java.util.List;
import javax.ws.rs.core.Response;


import com.bbva.ccol.transfers.business.dto.DTOIntInternalTransfer;
import com.bbva.ccol.transfers.business.dto.DTOIntItemizeTaxes;
import com.bbva.ccol.transfers.business.dto.DTOIntChargeAmount;
import com.bbva.ccol.transfers.business.dto.DTOIntIdentityDocument;
import com.bbva.ccol.transfers.business.dto.DTOIntSentMoney;
import com.bbva.ccol.transfers.business.dto.DTOIntHolder;
import com.bbva.ccol.transfers.business.dto.DTOIntDocumentType;
import com.bbva.ccol.transfers.business.dto.DTOIntReceiver;
import com.bbva.ccol.transfers.business.dto.DTOIntContract;
import com.bbva.ccol.transfers.business.dto.DTOIntExchangeRate;
import com.bbva.ccol.transfers.business.dto.DTOIntSender;
import com.bbva.ccol.transfers.business.dto.DTOIntItemizeFees;
import com.bbva.ccol.transfers.business.dto.DTOIntWhoPayFeeType;
import com.bbva.ccol.transfers.business.dto.DTOIntAmount;
import com.bbva.ccol.transfers.business.dto.DTOIntInternalTransferFees;
import com.bbva.ccol.transfers.business.dto.DTOIntReferences;
import com.bbva.ccol.transfers.business.dto.DTOIntTotalFees;
import com.bbva.ccol.transfers.business.dto.DTOIntContractNumberFormat;
import com.bbva.ccol.transfers.business.dto.DTOIntContractType;
import com.bbva.ccol.transfers.business.dto.DTOIntInternalTransferTaxes;
import com.bbva.ccol.transfers.facade.v01.dto.InternalTransfer;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.ibm.disthub2.impl.matching.selector.ParseException;



public interface ISrvIntTransfers {
	
	
	public DTOIntInternalTransfer createInternalTransfer(DTOIntInternalTransfer dtoIntInternalTransfer, String simulat)  throws BusinessServiceException, ParseException, java.text.ParseException;


	
}