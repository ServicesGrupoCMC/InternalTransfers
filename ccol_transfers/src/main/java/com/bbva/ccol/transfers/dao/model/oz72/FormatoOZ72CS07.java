package com.bbva.ccol.transfers.dao.model.oz72;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;


/**
 * Formato de datos <code>OZ72CS07</code> de la transacci&oacute;n <code>OZ72</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "OZ72CS07")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoOZ72CS07 {
	
	/**
	 * <p>Campo <code>NOTIFTY</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "NOTIFTY", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String notifty;
	
	/**
	 * <p>Campo <code>NOTIFNM</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "NOTIFNM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String notifnm;
	
	/**
	 * <p>Campo <code>NOTIFVL</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "NOTIFVL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 80, longitudMaxima = 80)
	private String notifvl;
	
	/**
	 * <p>Campo <code>NOTSTID</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "NOTSTID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String notstid;
	
	/**
	 * <p>Campo <code>NOTSTNM</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "NOTSTNM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String notstnm;
	
}