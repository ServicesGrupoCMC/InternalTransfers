
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "factor", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "factor", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Factor
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Value of the exchange for purchase or sale. According to the exchange type this value should be associated as a magnitude of the base currency for the sale or exchange currency for purchase", required = false)
    private Double value;
    @ApiModelProperty(value = "purchase or sale exchange ratio.Represents a percentage with the relation between the base and target currencies.", required = false)
    private Double ratio;

    public Factor() {
        //default constructor
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Double getRatio() {
        return ratio;
    }

    public void setRatio(Double ratio) {
        this.ratio = ratio;
    }

}
