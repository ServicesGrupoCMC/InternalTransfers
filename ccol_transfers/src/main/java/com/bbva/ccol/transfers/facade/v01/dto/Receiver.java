
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "receiver", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "receiver", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Receiver
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "User customizable alias for identifying the stored internal transfer receiver.", required = true)
    private String alias;
    @ApiModelProperty(value = "Contract name.", required = true)
    private Contract contract;
    @ApiModelProperty(value = "Holder of the contract that receives the internal transfer.", required = true)
    private Holder holder;

    public Receiver() {
        //default constructor
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public Holder getHolder() {
        return holder;
    }

    public void setHolder(Holder holder) {
        this.holder = holder;
    }

}
