
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.bbva.jee.arq.spring.core.servicing.utils.CalendarAdapter;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "exchangeRate", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "exchangeRate", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExchangeRate
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Base currency. String based on ISO-4217 for specifying the currency.", required = true)
    private String baseCurrency;
    @ApiModelProperty(value = "Currency exchange rate. String based on ISO-4217 for specifying the currency.", required = true)
    private String targetCurrency;
    @XmlJavaTypeAdapter(CalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    @ApiModelProperty(value = "Date exchange rate. Date when transfer has been made. String based on ISO-8601 date format.", required = true)
    private Date date;
    @ApiModelProperty(value = "Detailed info for the exchange rate", required = false)
    private Values values;

    public ExchangeRate() {
        //default constructor
    }

    public String getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public String getTargetCurrency() {
        return targetCurrency;
    }

    public void setTargetCurrency(String targetCurrency) {
        this.targetCurrency = targetCurrency;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Values getValues() {
        return values;
    }

    public void setValues(Values values) {
        this.values = values;
    }

}
