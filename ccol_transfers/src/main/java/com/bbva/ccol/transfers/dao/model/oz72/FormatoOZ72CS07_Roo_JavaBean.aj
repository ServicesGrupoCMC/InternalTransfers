// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.ccol.transfers.dao.model.oz72;

import java.lang.String;

privileged aspect FormatoOZ72CS07_Roo_JavaBean {
    
    public String FormatoOZ72CS07.getNotifty() {
        return this.notifty;
    }
    
    public void FormatoOZ72CS07.setNotifty(String notifty) {
        this.notifty = notifty;
    }
    
    public String FormatoOZ72CS07.getNotifnm() {
        return this.notifnm;
    }
    
    public void FormatoOZ72CS07.setNotifnm(String notifnm) {
        this.notifnm = notifnm;
    }
    
    public String FormatoOZ72CS07.getNotifvl() {
        return this.notifvl;
    }
    
    public void FormatoOZ72CS07.setNotifvl(String notifvl) {
        this.notifvl = notifvl;
    }
    
    public String FormatoOZ72CS07.getNotstid() {
        return this.notstid;
    }
    
    public void FormatoOZ72CS07.setNotstid(String notstid) {
        this.notstid = notstid;
    }
    
    public String FormatoOZ72CS07.getNotstnm() {
        return this.notstnm;
    }
    
    public void FormatoOZ72CS07.setNotstnm(String notstnm) {
        this.notstnm = notstnm;
    }
    
}
