package com.bbva.ccol.transfers.facade.v01;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.jaxrs.PATCH;

import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;

import com.bbva.ccol.transfers.facade.v01.dto.InternalTransfer;
import com.bbva.ccol.transfers.facade.v01.dto.ItemizeTaxes;
import com.bbva.ccol.transfers.facade.v01.dto.ChargeAmount;
import com.bbva.ccol.transfers.facade.v01.dto.IdentityDocument;
import com.bbva.ccol.transfers.facade.v01.dto.SentMoney;
import com.bbva.ccol.transfers.facade.v01.dto.Holder;
import com.bbva.ccol.transfers.facade.v01.dto.DocumentType;
import com.bbva.ccol.transfers.facade.v01.dto.Receiver;
import com.bbva.ccol.transfers.facade.v01.dto.Contract;
import com.bbva.ccol.transfers.facade.v01.dto.ExchangeRate;
import com.bbva.ccol.transfers.facade.v01.dto.Sender;
import com.bbva.ccol.transfers.facade.v01.dto.ItemizeFees;
import com.bbva.ccol.transfers.facade.v01.dto.WhoPayFeeType;
import com.bbva.ccol.transfers.facade.v01.dto.Amount;
import com.bbva.ccol.transfers.facade.v01.dto.InternalTransferFees;
import com.bbva.ccol.transfers.facade.v01.dto.References;
import com.bbva.ccol.transfers.facade.v01.dto.TotalFees;
import com.bbva.ccol.transfers.facade.v01.dto.ContractNumberFormat;
import com.bbva.ccol.transfers.facade.v01.dto.ContractType;
import com.bbva.ccol.transfers.facade.v01.dto.InternalTransferTaxes;
import com.bbva.ccol.transfers.facade.v01.mapper.Mapper;
import com.bbva.ccol.transfers.business.ISrvIntTransfers;
import com.bbva.ccol.transfers.business.impl.SrvIntTransfers;
import com.ibm.disthub2.impl.matching.selector.ParseException;


	
@Path("/V01")
@SN(registryID="SNCO1600040",logicalID="transfers")
@VN(vnn="V01")
@Api(value="/transfers/V01",description="Data common to all kind of transfers involving the emission and reception of a certain amount, securities, points, rewards or bonds accumulated in contracts, programs or any of the financial instruments that the bank offers on which consumers, customers or people have access. This transfer is expected to be executed immediately, or at least the consumer expects that the execution thereof is made the first opportunity that is technically possible, that's means that is not a scenario where the consumer schedule or deliberately postponed the execution of the transfer. For all cases where it intends to offer the consumer the ability to save data associated with any transfer for later execution you need to redirect them into the operations API")
@Produces({ MediaType.APPLICATION_JSON})
@Service

	
public class SrvTransfersV01 implements ISrvTransfersV01, com.bbva.jee.arq.spring.core.servicing.utils.ContextAware {

	private static I18nLog log = I18nLogFactory.getLogI18n(SrvTransfersV01.class,"META-INF/spring/i18n/log/mensajesLog");

	public HttpHeaders httpHeaders;
	
	@Autowired
	BusinessServicesToolKit bussinesToolKit;
	

	public UriInfo uriInfo;
	
	@Override
	public void setUriInfo(UriInfo ui) {
		this.uriInfo = ui;
		 
	}

	@Override
	public void setHttpHeaders(HttpHeaders httpHeaders) {
		this.httpHeaders = httpHeaders;
	}
	
	@Autowired
	ISrvIntTransfers srvIntTransfers;
	
	
	@ApiOperation(value="Method for creating a internal transfer.", notes="More text...",response=InternalTransfer.class)
	@ApiResponses(value = {
		@ApiResponse(code = -1, message = "aliasGCE1"),
		@ApiResponse(code = -1, message = "aliasGCE2"),
		@ApiResponse(code = 200, message = "Added Sucessfully", response=Response.class),
		@ApiResponse(code = 500, message = "Technical Error"),
	//	@ApiResponse(code = 215, message = "Resultado de simulacion del servicio"),
		@ApiResponse(code = 409, message = "Data not found")}


	)
	@POST
	@Consumes({ MediaType.APPLICATION_JSON})
	@SMC(registryID="SMCCO1600218",logicalID="createInternalTransfer")
	public Response createInternalTransfer( @ApiParam(value="Transfer to add") 	 @DefaultValue("") @QueryParam("Simulated") String simulat,InternalTransfer infoInternalTransfer) throws BusinessServiceException, ParseException, java.text.ParseException {
		//TODO: autogenerated, complete implementation using internal service
		
		//log.info(" addTransfer with	 id transfer: "+ infoInternalTransfer.getInternalTransferId());
		log.info(" addTransfer with	 id transfer: ");
		srvIntTransfers.createInternalTransfer (Mapper.dtoIntInternalTransfer(infoInternalTransfer), simulat);	//esto es para la entrada al dto interno	
		infoInternalTransfer.setInternalTransferId(infoInternalTransfer.getInternalTransferId());// esto es para salida a dto externo
//		
//		Deposit deposit = Mapper.depositMap(srvIntDeposit.createDeposit(Mapper.dtoIntDepositMap(infoDeposit)));
//		return deposit;
		
		return Response.ok(infoInternalTransfer).build();
	}


 
	}

