
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "holder", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "holder", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Holder
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Full name of the holder of the contract. This name can represent a natural or legal person that is the holder of the contract. Is expected a value as is recorded in the bank to which that contract belongs", required = true)
    private String holderName;
    @ApiModelProperty(value = "Documents used to identify the holder. It is all documents that the person was discharged in the state.", required = true)
    private List<IdentityDocument> identityDocuments;
    @ApiModelProperty(value = "Contact information type identifier.", required = true)
    private String contactType;
    @ApiModelProperty(value = "Contact information type name.", required = true)
    private String contactValue;

    public Holder() {
        //default constructor
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public List<IdentityDocument> getIdentityDocuments() {
        return identityDocuments;
    }

    public void setIdentityDocuments(List<IdentityDocument> identityDocuments) {
        this.identityDocuments = identityDocuments;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getContactValue() {
        return contactValue;
    }

    public void setContactValue(String contactValue) {
        this.contactValue = contactValue;
    }

}
