package com.bbva.ccol.transfers.dao.model.oz72;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;


/**
 * Formato de datos <code>OZ72CS05</code> de la transacci&oacute;n <code>OZ72</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "OZ72CS05")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoOZ72CS05 {
	
	/**
	 * <p>Campo <code>TXITMTY</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "TXITMTY", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
	private String txitmty;
	
	/**
	 * <p>Campo <code>TXITMNM</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "TXITMNM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
	private String txitmnm;
	
	/**
	 * <p>Campo <code>TXITMAM</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "TXITMAM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 17, longitudMaxima = 17)
	private String txitmam;
	
	/**
	 * <p>Campo <code>TXITMCU</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "TXITMCU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String txitmcu;
	
	/**
	 * <p>Campo <code>TXITMPR</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "TXITMPR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String txitmpr;
	
}