
package com.bbva.ccol.transfers.facade.v01.dto;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


import com.bbva.jee.arq.spring.core.servicing.utils.CalendarAdapter;
import com.bbva.jee.arq.spring.core.servicing.utils.DateAdapter;
import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "internalTransfer", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlType(name = "internalTransfer", namespace = "urn:com:bbva:ccol:transfers:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class InternalTransfer
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Identifier of the internal transfer.", required = false)
    private String internalTransferId;
    @ApiModelProperty(value = "Transfer short description.", required = false)
    private String concept;
    @ApiModelProperty(value = "Sender of the internal transfer. Represents information about the origin of the transfer which, in the case of a internal transfer, could be only a product", required = true)
    private Sender sender;
    @ApiModelProperty(value = "Receiver of the internal transfer. Represents information about the destination of the transfer which may include information about a contract and its holder", required = true)
    private Receiver receiver;
    @ApiModelProperty(value = "Amount of the transfer.", required = true)
    private List<SentMoney> sentMoney;
    @ApiModelProperty(value = "Total amount loaded in the origin contract, program or instrument.", required = true)
    private ChargeAmount chargeAmount;
    @XmlJavaTypeAdapter(DateAdapter.class)
    @XmlSchemaType(name = "dateTime")
    @ApiModelProperty(value = "Date and time of the transfer. String based on ISO-8601 date format.", required = true)
    private  Date date;
    @XmlJavaTypeAdapter(DateAdapter.class)
    @XmlSchemaType(name = "dateTime")
    @ApiModelProperty(value = "Date when the transfer was last modified. String based on ISO-8601 date format.", required = true)
    private Date modifiedDate;
    @XmlJavaTypeAdapter(DateAdapter.class)
    @XmlSchemaType(name = "dateTime")
    @ApiModelProperty(value = "Date on which the transfer becomes part of the bank accounts.", required = true)
    private Date accountingDate;
    @ApiModelProperty(value = "Commissions transfer.", required = true)
    private InternalTransferFees fees;
    @ApiModelProperty(value = "Transfer Tax.", required = true)
    private InternalTransferTaxes taxes;
    @ApiModelProperty(value = "Exchenge rate value.", required = true)
    private ExchangeRate exchangeRate;
    @ApiModelProperty(value = "Exchenge rate value.", required = true)
    private List<References> references;
    @ApiModelProperty(value = "Reference number specified by the Sender. It is a common banking practice to use bank reference numbers for transactions with the aim of tracking operations", required = true)
    private String customExternalReference;
    @ApiModelProperty(value = "Reference number to the operation performed.", required = true)
    private String operationNumber;
    @ApiModelProperty(value = "Management allusive notification to the message you want to inform the transfer to all interested partners.", required = false)
    private List<Notification> notification;
    @ApiModelProperty(value = "Status associated with the transfer.", required = false)
    private Status status;

    public InternalTransfer() {
        //default constructor
    }

    public String getInternalTransferId() {
        return internalTransferId;
    }

    public void setInternalTransferId(String internalTransferId) {
        this.internalTransferId = internalTransferId;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public Receiver getReceiver() {
        return receiver;
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    public List<SentMoney> getSentMoney() {
        return sentMoney;
    }

    public void setSentMoney(List<SentMoney> sentMoney) {
        this.sentMoney = sentMoney;
    }

    public ChargeAmount getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(ChargeAmount chargeAmount) {
        this.chargeAmount = chargeAmount;
    }
  
    public Date getDate() {
    	
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Date getAccountingDate() {
        return accountingDate;
    }

    public void setAccountingDate(Date accountingDate) {
        this.accountingDate = accountingDate;
    }

    public InternalTransferFees getFees() {
        return fees;
    }

    public void setFees(InternalTransferFees fees) {
        this.fees = fees;
    }

    public InternalTransferTaxes getTaxes() {
        return taxes;
    }

    public void setTaxes(InternalTransferTaxes taxes) {
        this.taxes = taxes;
    }

    public ExchangeRate getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(ExchangeRate exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public List<References> getReferences() {
        return references;
    }

    public void setReferences(List<References> references) {
        this.references = references;
    }

    public String getCustomExternalReference() {
        return customExternalReference;
    }

    public void setCustomExternalReference(String customExternalReference) {
        this.customExternalReference = customExternalReference;
    }

    public String getOperationNumber() {
        return operationNumber;
    }

    public void setOperationNumber(String operationNumber) {
        this.operationNumber = operationNumber;
    }

    public List<Notification> getNotification() {
        return notification;
    }

    public void setNotification(List<Notification> notification) {
        this.notification = notification;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
